﻿namespace RedisIssue
{
    public class CacheProviderSettings
    {
        public string ConnectionString { get; set; }
        public List<string> ReadOnlyConnectionStrings { get; set; }
        public string SentinelHost { get; set; } = "";
        public int ConnectionTimeout { get; set; } = 1000;
        public int SentinelConnectionRetryInterval { get; set; } = 60000;
        public string MasterGroupName { get; set; } = "mymaster";
    }
}
