﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ServiceStack;

namespace RedisIssue
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("Configs/cache.json");

            IConfiguration configuration = builder.Build();

            var cacheProviderSettings = new CacheProviderSettings();
            configuration.GetSection("CacheProviderSettings").Bind(cacheProviderSettings);

            var serviceProvider = new ServiceCollection()
            .AddSingleton(typeof(CacheProviderSettings), cacheProviderSettings)
            .AddSingleton<ServiceStackRedisPool>()
            .AddScoped<ICacheClient, RedisCacheClientServiceStack>()
            .BuildServiceProvider();

            for (int i = 1; i <= 1000; i++)
            {
                using (var scope = serviceProvider.CreateScope())
                {
                    var cacheClient = scope.ServiceProvider.GetRequiredService<ICacheClient>();
                    cacheClient.Store($"Test-{i}", $"Data-{i}");

                    if (i > 1)
                    {
                        var data = cacheClient.Get<string>($"Test-{i - 1}");
                        Console.WriteLine("Data from cache : " + data);
                    }

                    Console.WriteLine("=================================================");
                    await Task.Delay(1000);
                }
            }
        }
    }
}