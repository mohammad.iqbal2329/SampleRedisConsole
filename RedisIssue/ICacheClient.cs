﻿using Newtonsoft.Json.Linq;

namespace RedisIssue
{
    public interface ICacheClient
    {
        void Store(string key, object value, double ttlSeconds);
        void Store(string key, object value, TimeSpan ttl);
        void Store(string key, object value);
        void Store(string key, object value, double ttlSeconds, string applicationName);
        void Store(string key, object value, TimeSpan ttl, string applicationName);
        void Store(string key, object value, string applicationName);
        T Get<T>(string key);
        JToken Get(string key);
        JToken Get(string key, string applicationName);
        T Get<T>(string key, string applicationName);
        Dictionary<string, object> GetAll();
        Dictionary<string, object> GetAll(string pattern);
        Dictionary<string, object> GetAll(string pattern, string applicationName);
        Dictionary<string, JToken> GetAllAsJToken();
        Dictionary<string, JToken> GetAllAsJToken(string pattern);
        Dictionary<string, JToken> GetAllAsJToken(string pattern, string applicationName);
        TimeSpan? GetTtl(string key);
        TimeSpan? GetTtl(string key, string applicationName);
        void Clear(string key);
        void Clear(string key, string applicationName);
        void ClearByKeyPrefix(string prefix);
        void ClearAll();
    }
}
