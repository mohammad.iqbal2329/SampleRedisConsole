﻿using ServiceStack.Redis;
using ServiceStack.Text;
using System.Text.Json;

namespace RedisIssue
{
    public class ServiceStackRedisPool : IDisposable
    {
        private IRedisClientsManager _redisManagerPool;
        private readonly CacheProviderSettings _configs;
        private List<string> _sentinelHosts;
        private bool _isUseSentinelHost;
        private RedisSentinel _redisSentinel;

        private const string SELECTED_PROVIDER = "ServiceStackRedisProvider";

        public ServiceStackRedisPool(CacheProviderSettings configs)
        {
            _configs = configs;

            RedisConfig.DefaultConnectTimeout = _configs.ConnectionTimeout;
            RedisConfig.DefaultRetryTimeout = _configs.ConnectionTimeout;

            _GetSentinelHosts();

            _isUseSentinelHost = _sentinelHosts.Any();
            if (_isUseSentinelHost)
            {
                _ConnectToSentinelServices();
            }
            else
            {
                _ConnectToSingleHostService();
            }
        }

        private void _GetSentinelHosts()
        {
            _sentinelHosts = _configs.SentinelHost
                    .Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries)
                    .Distinct()
                    .ToList();
        }

        private void _ConnectToSentinelServices()
        {
            Task.Run(() =>
            {
                while (!_CreateConnectionToSentinel())
                {
                    Task.Delay(_configs.SentinelConnectionRetryInterval);
                }

                Console.WriteLine("Successfully connecting to sentinel");
            });
        }

        private void _ConnectToSingleHostService()
        {
            //TODO: When use host
            if (!string.IsNullOrEmpty(_configs.ConnectionString))
            {
                try
                {
                    List<string> ReadOnlyHost = _configs.ReadOnlyConnectionStrings?.Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

                    //With ReadOnly Host (Optional)
                    if (ReadOnlyHost != null && ReadOnlyHost.Count > 0)
                    {
                        List<string> ReadWriteHost = new List<string>
                        {
                            _configs.ConnectionString
                        };

                        _redisManagerPool = new PooledRedisClientManager(ReadWriteHost, ReadOnlyHost);
                    }
                    //Without ReadOnly Host
                    else
                    {
                        _redisManagerPool = new PooledRedisClientManager(_configs.ConnectionString);
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine("Failed to connect to Redis instance.\n" + e.Message, e);
                    throw e;
                }
            }
            else
            {
                throw new Exception("Make sure configuration for SentinelHost or ConnectionString available");
            }
        }

        private bool _CreateConnectionToSentinel()
        {
            try
            {
                Console.WriteLine($"Try connect to sentinel service");

                Console.WriteLine("Creating redis sentinel");
                _redisSentinel = new RedisSentinel(_sentinelHosts, _configs.MasterGroupName);
                Console.WriteLine("Created redis sentinel");

                Console.WriteLine("Start redis sentinel");
                _redisManagerPool = _redisSentinel.Start();
                Console.WriteLine("Start redis sentinel");

                return _isUseSentinelHost;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Failed using host sentinel service with MasterGroupName config {_configs.MasterGroupName}: {ex.Message}", ex);
                return false;
            }
        }

        public IRedisClientsManager GetManagerPool()
        {
            if (_redisManagerPool == null)
            {
                throw new Exception("Current connection to sentinel is not available");
            }

            return _redisManagerPool;
        }

        public string GetInfo()
        {
            var internalInfo = ((PooledRedisClientManager)_redisManagerPool)?.GetStats();
            return internalInfo?.Dump();
        }

        public void Dispose()
        {
            if (_redisManagerPool != null)
            {
                Console.WriteLine("Disposing redisManagerPool");
                _redisManagerPool.Dispose();
                Console.WriteLine("Disposed redisManagerPool");
            }

            if (_redisSentinel != null)
            {
                Console.WriteLine("Disposing redisSentinel");
                _redisSentinel.Dispose();
                Console.WriteLine("Disposed redisSentinel");
            }
        }
    }
}
