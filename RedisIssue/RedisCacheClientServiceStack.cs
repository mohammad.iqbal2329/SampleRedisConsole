﻿using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ServiceStack;

namespace RedisIssue
{
    public class RedisCacheClientServiceStack : ICacheClient
    {
        private class RedisValueType
        {
            public string ValueJson { get; set; }
            public Type ValueType { get; set; }
        }

        private readonly ServiceStackRedisPool _redisPool;
        private readonly string _applicationName;
        private ServiceProvider redisPool;

        public RedisCacheClientServiceStack(ServiceStackRedisPool redisPool)
        {
            _applicationName = "RedisIssue";
            _redisPool = redisPool;
        }

        public void Store(string key, object value, double ttlSeconds)
        {
            try
            {
                string dbkey = _GetDbKey(key);
                Console.WriteLine($"Start storing cache with key : {key}");
                _Store(dbkey, value, TimeSpan.FromSeconds(ttlSeconds));
                Console.WriteLine($"Successfully store cache with key: {key}, time to live: {ttlSeconds}");
            }
            catch (Exception e)
            {
                var workItems = $"(Threads={ThreadPool.ThreadCount},QueuedItems={ThreadPool.PendingWorkItemCount},CompletedItems={ThreadPool.CompletedWorkItemCount},Timers={Timer.ActiveCount})";
                Console.WriteLine("Redis Info :");
                Console.WriteLine(_redisPool.GetInfo());
                Console.WriteLine($"Failed to store cache with key '{key}', reason: {e.Message}, with Thread Info: {workItems}", e);
                return;
            }
        }

        public void Store(string key, object value, TimeSpan ttl)
        {
            try
            {
                string dbkey = _GetDbKey(key);
                _Store(dbkey, value, ttl);
                Console.WriteLine($"Successfully store cache with key: {key}, time span: {Convert.ToString(ttl.TotalMilliseconds)}");
            }
            catch (Exception e)
            {
                var workItems = $"(Threads={ThreadPool.ThreadCount},QueuedItems={ThreadPool.PendingWorkItemCount},CompletedItems={ThreadPool.CompletedWorkItemCount},Timers={Timer.ActiveCount})";
                Console.WriteLine("Redis Info :");
                Console.WriteLine(_redisPool.GetInfo());
                Console.WriteLine($"Failed to store cache with key '{key}', reason: {e.Message}, with Thread Info: {workItems}", e);
                return;
            }
        }

        public void Store(string key, object value)
        {
            try
            {
                string dbkey = _GetDbKey(key);
                Console.WriteLine($"Start storing cache with key : {key}");
                _Store(dbkey, value);
                Console.WriteLine($"Successfully store cache with key: {key}");
            }
            catch (Exception e)
            {
                var workItems = $"(Threads={ThreadPool.ThreadCount},QueuedItems={ThreadPool.PendingWorkItemCount},CompletedItems={ThreadPool.CompletedWorkItemCount},Timers={Timer.ActiveCount})";
                Console.WriteLine("Redis Info :");
                Console.WriteLine(_redisPool.GetInfo());
                Console.WriteLine($"Failed to store cache with key '{key}', reason: {e.Message}, with Thread Info: {workItems}", e);
                return;
            }
        }

        public void Store(string key, object value, double ttlSeconds, string applicationName)
        {
            try
            {
                string dbkey = _GetDbKey(key, applicationName);
                Console.WriteLine($"Start storing cache with key : {key} and applicationName : {applicationName}");
                _Store(dbkey, value, TimeSpan.FromSeconds(ttlSeconds));
                Console.WriteLine($"Successfully store cache with key: {key} and applicationName : {applicationName}, time to live: {ttlSeconds}");
            }
            catch (Exception e)
            {
                var workItems = $"(Threads={ThreadPool.ThreadCount},QueuedItems={ThreadPool.PendingWorkItemCount},CompletedItems={ThreadPool.CompletedWorkItemCount},Timers={Timer.ActiveCount})";
                Console.WriteLine("Redis Info :");
                Console.WriteLine(_redisPool.GetInfo());
                Console.WriteLine($"Failed to store cache with key '{key}', reason: {e.Message}, with Thread Info: {workItems}", e);
                return;
            }
        }

        public void Store(string key, object value, TimeSpan ttl, string applicationName)
        {
            try
            {
                string dbkey = _GetDbKey(key, applicationName);
                Console.WriteLine($"Start storing cache with key : {key} and applicationName : {applicationName}");
                _Store(dbkey, value, ttl);
                Console.WriteLine($"Successfully store cache with key: {key} and applicationName : {applicationName}, time to live: {ttl}");
            }
            catch (Exception e)
            {
                var workItems = $"(Threads={ThreadPool.ThreadCount},QueuedItems={ThreadPool.PendingWorkItemCount},CompletedItems={ThreadPool.CompletedWorkItemCount},Timers={Timer.ActiveCount})";
                Console.WriteLine("Redis Info :");
                Console.WriteLine(_redisPool.GetInfo());
                Console.WriteLine($"Failed to store cache with key '{key}', reason: {e.Message}, with Thread Info: {workItems}", e);
                return;
            }
        }

        public void Store(string key, object value, string applicationName)
        {
            try
            {
                string dbkey = _GetDbKey(key, applicationName);

                Console.WriteLine($"Start storing cache with key : {key} and applicationName : {applicationName}");
                _Store(dbkey, value);
                Console.WriteLine($"Successfully store cache with key: {key} and applicationName : {applicationName}");
            }
            catch (Exception e)
            {
                var workItems = $"(Threads={ThreadPool.ThreadCount},QueuedItems={ThreadPool.PendingWorkItemCount},CompletedItems={ThreadPool.CompletedWorkItemCount},Timers={Timer.ActiveCount})";
                Console.WriteLine("Redis Info :");
                Console.WriteLine(_redisPool.GetInfo());
                Console.WriteLine($"Failed to store cache with key '{key}', reason: {e.Message}, with Thread Info: {workItems}", e);
                return;
            }
        }

        public JToken Get(string key)
        {
            try
            {
                string dbkey = _GetDbKey(key);
                string valueJson = null;
                using (var client = _redisPool.GetManagerPool().GetReadOnlyClient())
                {
                    Console.WriteLine($"Use Host: {client.Host}:{client.Port}");
                    Console.WriteLine($"Get Role: {client.GetServerRole()}");

                    valueJson = client.GetValue(dbkey);
                }

                if (valueJson == null)
                {
                    return null;
                }

                JToken jToken = JToken.Parse(valueJson);
                string jsonValue = jToken.SelectToken(nameof(RedisValueType.ValueJson)).Value<string>();
                Console.WriteLine($"Successfully retrieve cache with key: {key}");
                return JToken.Parse(jsonValue);
            }
            catch (Exception e)
            {
                var workItems = $"(Threads={ThreadPool.ThreadCount},QueuedItems={ThreadPool.PendingWorkItemCount},CompletedItems={ThreadPool.CompletedWorkItemCount},Timers={Timer.ActiveCount})";
                Console.WriteLine("Redis Info :");
                Console.WriteLine(_redisPool.GetInfo());
                Console.WriteLine($"Failed to retrieve cache with key '{key}', reason: {e.Message}, with Thread Info: {workItems} ", e);
                return null;
            }
        }

        public JToken Get(string key, string applicationName)
        {
            try
            {
                string dbkey = _GetDbKey(key, applicationName);
                string valueJson = null;
                using (var client = _redisPool.GetManagerPool().GetReadOnlyClient())
                {
                    Console.WriteLine($"Use Host: {client.Host}:{client.Port}");
                    Console.WriteLine($"Get Role: {client.GetServerRole()}");

                    valueJson = client.GetValue(dbkey);
                }

                if (valueJson == null)
                {
                    return null;
                }

                JToken jToken = JToken.Parse(valueJson);
                string jsonValue = jToken.SelectToken(nameof(RedisValueType.ValueJson)).Value<string>();
                Console.WriteLine($"Successfully retrieve cache with key: {key}");
                return JToken.Parse(jsonValue);
            }
            catch (Exception e)
            {
                var workItems = $"(Threads={ThreadPool.ThreadCount},QueuedItems={ThreadPool.PendingWorkItemCount},CompletedItems={ThreadPool.CompletedWorkItemCount},Timers={Timer.ActiveCount})";
                Console.WriteLine("Redis Info :");
                Console.WriteLine(_redisPool.GetInfo());
                Console.WriteLine($"Failed to retrieve cache with key '{key}', reason: {e.Message}, with Thread Info: {workItems} ", e);
                return null;
            }
        }

        public T Get<T>(string key)
        {
            try
            {
                string dbkey = _GetDbKey(key);
                string valueJson = null;
                using (var client = _redisPool.GetManagerPool().GetReadOnlyClient())
                {
                    Console.WriteLine($"Use Host: {client.Host}:{client.Port}");
                    Console.WriteLine($"Get Role: {client.GetServerRole()}");

                    valueJson = client.GetValue(dbkey);
                }

                if (valueJson == null)
                {
                    return default(T);
                }

                RedisValueType wrapper = JsonConvert.DeserializeObject<RedisValueType>(valueJson);
                Console.WriteLine($"Successfully retrieve cache with key: {key}");

                return JsonConvert.DeserializeObject<T>(wrapper.ValueJson);
            }
            catch (Exception e)
            {
                var workItems = $"(Threads={ThreadPool.ThreadCount},QueuedItems={ThreadPool.PendingWorkItemCount},CompletedItems={ThreadPool.CompletedWorkItemCount},Timers={Timer.ActiveCount})";
                Console.WriteLine("Redis Info :");
                Console.WriteLine(_redisPool.GetInfo());
                Console.WriteLine($"Failed to retrieve cache with key '{key}', reason: {e.Message}, with Thread Info: {workItems}", e);
                return default(T);
            }
        }

        public T Get<T>(string key, string applicationName)
        {
            try
            {
                string dbkey = _GetDbKey(key, applicationName);
                string valueJson = null;
                using (var client = _redisPool.GetManagerPool().GetReadOnlyClient())
                {
                    Console.WriteLine($"Use Host: {client.Host}:{client.Port}");
                    Console.WriteLine($"Get Role: {client.GetServerRole()}");

                    valueJson = client.GetValue(dbkey);
                }

                if (valueJson == null)
                {
                    return default(T);
                }

                RedisValueType wrapper = JsonConvert.DeserializeObject<RedisValueType>(valueJson);
                Console.WriteLine($"Successfully retrieve cache with key: {key}");

                return JsonConvert.DeserializeObject<T>(wrapper.ValueJson);
            }
            catch (Exception e)
            {
                var workItems = $"(Threads={ThreadPool.ThreadCount},QueuedItems={ThreadPool.PendingWorkItemCount},CompletedItems={ThreadPool.CompletedWorkItemCount},Timers={Timer.ActiveCount})";
                Console.WriteLine("Redis Info :");
                Console.WriteLine(_redisPool.GetInfo());
                Console.WriteLine($"Failed to retrieve cache with key '{key}', reason: {e.Message}, with Thread Info: {workItems}", e);
                return default(T);
            }
        }

        // Returning IEnumerable from ScanAllKeys for lazy Enumerable
        // Will reduce load of memory
        // Reference https://github.com/ServiceStack/ServiceStack.Redis#scan-apis
        #region Spesific Method in Service Stack
        public IEnumerable<string> GetKeysByPattern(string pattern, int pageSize = 1000)
        {
            try
            {
                string keyPattern = _GetDbKey(pattern);
                IEnumerable<string> result = Enumerable.Empty<string>();
                using (var client = _redisPool.GetManagerPool().GetReadOnlyClient())
                {
                    Console.WriteLine($"Use Host: {client.Host}:{client.Port}");
                    Console.WriteLine($"Get Role: {client.GetServerRole()}");

                    var keys = client.ScanAllKeys(pattern: keyPattern, pageSize: pageSize).Take(pageSize);
                    result = keys;
                }

                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Failed to retrieve keys with pattern '{pattern}', reason : {e.Message}", e);
                return Enumerable.Empty<string>();
            }
        }


        public IEnumerable<string> GetAllKeysByPattern(string pattern)
        {
            try
            {
                string keyPattern = _GetDbKey(pattern);
                IEnumerable<string> result = Enumerable.Empty<string>();
                using (var client = _redisPool.GetManagerPool().GetReadOnlyClient())
                {
                    Console.WriteLine($"Use Host: {client.Host}:{client.Port}");
                    Console.WriteLine($"Get Role: {client.GetServerRole()}");

                    var keys = client.GetKeysByPattern(keyPattern);
                    result = keys;
                }

                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Failed to retrieve keys with pattern '{pattern}', reason : {e.Message}", e);
                return Enumerable.Empty<string>();
            }
        }

        public IEnumerable<string> GetKeysByPrefix(string prefix, int pageSize = 1000)
        {
            try
            {
                string keyPattern = _GetDbKey(prefix);
                string pref = keyPattern + "*";
                IEnumerable<string> result = Enumerable.Empty<string>();
                using (var client = _redisPool.GetManagerPool().GetReadOnlyClient())
                {
                    Console.WriteLine($"Use Host: {client.Host}:{client.Port}");
                    Console.WriteLine($"Get Role: {client.GetServerRole()}");

                    var keys = client.ScanAllKeys(pattern: pref, pageSize: pageSize).Take(pageSize);
                    result = keys;
                }

                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Failed to retrieve keys with prefix '{prefix}', reason : {e.Message}", e);
                return Enumerable.Empty<string>();
            }
        }


        public IEnumerable<string> GetAllKeysByPrefix(string prefix)
        {
            try
            {
                string keyPattern = _GetDbKey(prefix);
                string pref = keyPattern + "*";
                IEnumerable<string> result = Enumerable.Empty<string>();
                using (var client = _redisPool.GetManagerPool().GetReadOnlyClient())
                {
                    Console.WriteLine($"Use Host: {client.Host}:{client.Port}");
                    Console.WriteLine($"Get Role: {client.GetServerRole()}");

                    var keys = client.GetKeysByPattern(pref);
                    result = keys;
                }

                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Failed to retrieve keys with prefix '{prefix}', reason : {e.Message}", e);
                return Enumerable.Empty<string>();
            }
        }
        #endregion

        public Dictionary<string, JToken> GetAllAsJToken()
        {
            try
            {
                string keyPattern = _GetDbKey("*");
                return _GetAllAsJToken(keyPattern);
            }
            catch (Exception e)
            {
                var workItems = $"(Threads={ThreadPool.ThreadCount},QueuedItems={ThreadPool.PendingWorkItemCount},CompletedItems={ThreadPool.CompletedWorkItemCount},Timers={Timer.ActiveCount})";
                Console.WriteLine("Redis Info :");
                Console.WriteLine(_redisPool.GetInfo());
                Console.WriteLine($"Failed to retrieve all cache, reason: {e.Message}, with Thread Info: {workItems}", e);
                return new Dictionary<string, JToken>();
            }
        }
        public Dictionary<string, object> GetAll()
        {
            try
            {
                string keyPattern = _GetDbKey("*");
                return _GetAll(keyPattern);
            }
            catch (Exception e)
            {
                var workItems = $"(Threads={ThreadPool.ThreadCount},QueuedItems={ThreadPool.PendingWorkItemCount},CompletedItems={ThreadPool.CompletedWorkItemCount},Timers={Timer.ActiveCount})";
                Console.WriteLine("Redis Info :");
                Console.WriteLine(_redisPool.GetInfo());
                Console.WriteLine($"Failed to retrieve all cache, reason: {e.Message}, with Thread Info: {workItems}", e);
                return new Dictionary<string, object>();
            }
        }

        public TimeSpan? GetTtl(string key)
        {
            try
            {
                string dbkey = _GetDbKey(key);
                using (var client = _redisPool.GetManagerPool().GetReadOnlyClient())
                {
                    Console.WriteLine($"Use Host: {client.Host}:{client.Port}");
                    Console.WriteLine($"Get Role: {client.GetServerRole()}");

                    return client.GetTimeToLive(dbkey);
                }
            }
            catch (Exception e)
            {
                var workItems = $"(Threads={ThreadPool.ThreadCount},QueuedItems={ThreadPool.PendingWorkItemCount},CompletedItems={ThreadPool.CompletedWorkItemCount},Timers={Timer.ActiveCount})";
                Console.WriteLine("Redis Info :");
                Console.WriteLine(_redisPool.GetInfo());
                Console.WriteLine($"Failed to retrieve ttl of cache with key '{key}', reason : {e.Message}, with Thread Info: {workItems}", e);
                return null;
            }
        }

        public TimeSpan? GetTtl(string key, string applicationName)
        {
            try
            {
                string dbkey = _GetDbKey(key, applicationName);
                using (var client = _redisPool.GetManagerPool().GetReadOnlyClient())
                {
                    Console.WriteLine($"Use Host: {client.Host}:{client.Port}");
                    Console.WriteLine($"Get Role: {client.GetServerRole()}");

                    return client.GetTimeToLive(dbkey);
                }
            }
            catch (Exception e)
            {
                var workItems = $"(Threads={ThreadPool.ThreadCount},QueuedItems={ThreadPool.PendingWorkItemCount},CompletedItems={ThreadPool.CompletedWorkItemCount},Timers={Timer.ActiveCount})";
                Console.WriteLine("Redis Info :");
                Console.WriteLine(_redisPool.GetInfo());
                Console.WriteLine($"Failed to retrieve ttl of cache with key '{key}', reason : {e.Message}, with Thread Info: {workItems}", e);
                return null;
            }
        }

        public void Clear(string key)
        {
            try
            {
                string dbkey = _GetDbKey(key);
                using (var client = _redisPool.GetManagerPool().GetClient())
                {
                    Console.WriteLine($"Use Host: {client.Host}:{client.Port}");
                    Console.WriteLine($"Get Role: {client.GetServerRole()}");

                    client.Remove(dbkey);
                }

                Console.WriteLine($"Clear key:{key}");
            }
            catch (Exception e)
            {
                var workItems = $"(Threads={ThreadPool.ThreadCount},QueuedItems={ThreadPool.PendingWorkItemCount},CompletedItems={ThreadPool.CompletedWorkItemCount},Timers={Timer.ActiveCount})";
                Console.WriteLine("Redis Info :");
                Console.WriteLine(_redisPool.GetInfo());
                Console.WriteLine($"Failed to clear cache with key '{key}', reason : {e.Message}, with Thread Info: {workItems}", e);
                return;
            }
        }

        public void Clear(string key, string applicationName)
        {
            try
            {
                string dbkey = _GetDbKey(key, applicationName);
                using (var client = _redisPool.GetManagerPool().GetClient())
                {
                    Console.WriteLine($"Use Host: {client.Host}:{client.Port}");
                    Console.WriteLine($"Get Role: {client.GetServerRole()}");

                    client.Remove(dbkey);
                }

                Console.WriteLine($"Clear key:{key}");
            }
            catch (Exception e)
            {
                var workItems = $"(Threads={ThreadPool.ThreadCount},QueuedItems={ThreadPool.PendingWorkItemCount},CompletedItems={ThreadPool.CompletedWorkItemCount},Timers={Timer.ActiveCount})";
                Console.WriteLine("Redis Info :");
                Console.WriteLine(_redisPool.GetInfo());
                Console.WriteLine($"Failed to clear cache with key '{key}', reason : {e.Message}, with Thread Info: {workItems}", e);
                return;
            }
        }

        public void ClearByKeyPrefix(string prefix)
        {
            try
            {
                string keyPattern = _GetDbKey(prefix + "*");
                using (var client = _redisPool.GetManagerPool().GetClient())
                {
                    Console.WriteLine($"Use Host: {client.Host}:{client.Port}");
                    Console.WriteLine($"Get Role: {client.GetServerRole()}");

                    var keys = client.GetKeysByPattern(keyPattern).ToList();
                    client.RemoveAll(keys);
                }
                Console.WriteLine($"Clear prefix in key:{prefix}");
            }
            catch (Exception e)
            {
                var workItems = $"(Threads={ThreadPool.ThreadCount},QueuedItems={ThreadPool.PendingWorkItemCount},CompletedItems={ThreadPool.CompletedWorkItemCount},Timers={Timer.ActiveCount})";
                Console.WriteLine("Redis Info :");
                Console.WriteLine(_redisPool.GetInfo());
                Console.WriteLine($"Failed to clear cache with prefix '{prefix}', reason : {e.Message}, with Thread Info: {workItems}", e);
                return;
            }
        }

        public void ClearAll()
        {
            try
            {
                using (var client = _redisPool.GetManagerPool().GetClient())
                {
                    Console.WriteLine($"Use Host: {client.Host}:{client.Port}");
                    Console.WriteLine($"Get Role: {client.GetServerRole()}");

                    client.FlushDb();
                }
                Console.WriteLine($"Clear all");
            }
            catch (Exception e)
            {
                var workItems = $"(Threads={ThreadPool.ThreadCount},QueuedItems={ThreadPool.PendingWorkItemCount},CompletedItems={ThreadPool.CompletedWorkItemCount},Timers={Timer.ActiveCount})";
                Console.WriteLine("Redis Info :");
                Console.WriteLine(_redisPool.GetInfo());
                Console.WriteLine($"Failed to clear all cache, reason : {e.Message}, with Thread Info: {workItems}", e);
                return;
            }
        }

        public Dictionary<string, object> GetAll(string pattern)
        {
            try
            {
                Console.WriteLine($"Starting GetAll(pattern) with pattern '{pattern}'");
                string keyPattern = _GetDbKey(pattern);
                return _GetAll(keyPattern);
            }
            catch (Exception e)
            {
                var workItems = $"(Threads={ThreadPool.ThreadCount},QueuedItems={ThreadPool.PendingWorkItemCount},CompletedItems={ThreadPool.CompletedWorkItemCount},Timers={Timer.ActiveCount})";
                Console.WriteLine("Redis Info :");
                Console.WriteLine(_redisPool.GetInfo());
                Console.WriteLine($"Failed to retrieve all cache with pattern '{pattern}', reason : {e.Message}, with Thread Info: {workItems}", e);
                return new Dictionary<string, object>();
            }
        }

        public Dictionary<string, JToken> GetAllAsJToken(string pattern)
        {
            try
            {
                Console.WriteLine($"Starting GetAllAsJToken(pattern) with pattern '{pattern}'");
                string keyPattern = _GetDbKey(pattern);
                return _GetAllAsJToken(keyPattern);
            }
            catch (Exception e)
            {
                var workItems = $"(Threads={ThreadPool.ThreadCount},QueuedItems={ThreadPool.PendingWorkItemCount},CompletedItems={ThreadPool.CompletedWorkItemCount},Timers={Timer.ActiveCount})";
                Console.WriteLine("Redis Info :");
                Console.WriteLine(_redisPool.GetInfo());
                Console.WriteLine($"Failed to retrieve all cache with pattern '{pattern}', reason : {e.Message}, with Thread Info: {workItems}", e);
                return new Dictionary<string, JToken>();
            }
        }

        public Dictionary<string, object> GetAll(string pattern, string applicationName)
        {
            try
            {
                Console.WriteLine($"Starting GetAll(pattern) with pattern '{pattern}' and applicationName '{applicationName}' ");
                string keyPattern = _GetDbKey(pattern, applicationName);
                return _GetAll(keyPattern);
            }
            catch (Exception e)
            {
                var workItems = $"(Threads={ThreadPool.ThreadCount},QueuedItems={ThreadPool.PendingWorkItemCount},CompletedItems={ThreadPool.CompletedWorkItemCount},Timers={Timer.ActiveCount})";
                Console.WriteLine("Redis Info :");
                Console.WriteLine(_redisPool.GetInfo());
                Console.WriteLine($"Failed to retrieve all cache with pattern '{pattern}', reason : {e.Message}, with Thread Info: {workItems}", e);
                return new Dictionary<string, object>();
            }
        }

        public Dictionary<string, JToken> GetAllAsJToken(string pattern, string applicationName)
        {
            try
            {
                Console.WriteLine($"Starting GetAllAsJToken(pattern) with pattern '{pattern}' and applicationName '{applicationName}'");
                string keyPattern = _GetDbKey(pattern, applicationName);
                return _GetAllAsJToken(keyPattern);
            }
            catch (Exception e)
            {
                var workItems = $"(Threads={ThreadPool.ThreadCount},QueuedItems={ThreadPool.PendingWorkItemCount},CompletedItems={ThreadPool.CompletedWorkItemCount},Timers={Timer.ActiveCount})";
                Console.WriteLine("Redis Info :");
                Console.WriteLine(_redisPool.GetInfo());
                Console.WriteLine($"Failed to retrieve all cache with pattern '{pattern}', reason : {e.Message}, with Thread Info: {workItems}", e);
                return new Dictionary<string, JToken>();
            }
        }

        #region Private Method(s)

        private string _GetDbKey(string key, string applicationName = "")
        {
            if (string.IsNullOrEmpty(applicationName))
            {
                return _applicationName + "." + key;
            }

            return applicationName + "." + key;
        }

        private void _Store(string key, object value, TimeSpan ttl)
        {
            using (var client = _redisPool.GetManagerPool().GetClient())
            {
                Console.WriteLine($"Use Host: {client.Host}:{client.Port}");
                Console.WriteLine($"Get Role: {client.GetServerRole()}");

                var redisValueType = new RedisValueType
                {
                    ValueJson = value.ToJson(),
                    ValueType = value.GetType()
                }.ToJson();
                client.SetValue(key, redisValueType, ttl);
            }
        }

        private void _Store(string key, object value)
        {
            using (var client = _redisPool.GetManagerPool().GetClient())
            {
                Console.WriteLine($"Use Host: {client.Host}:{client.Port}");
                Console.WriteLine($"Get Role: {client.GetServerRole()}");

                var redisValueType = new RedisValueType
                {
                    ValueJson = value.ToJson(),
                    ValueType = value.GetType()
                }.ToJson();

                client.SetValue(key, redisValueType);
            }
        }

        private Dictionary<string, object> _GetAll(string keyPattern)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();

            Console.WriteLine($"Start preparing redis client");
            using (var client = _redisPool.GetManagerPool().GetReadOnlyClient())
            {
                Console.WriteLine($"Use Host: {client.Host}:{client.Port}");
                Console.WriteLine($"Get Role: {client.GetServerRole()}");

                Console.WriteLine($"Start retrieving keys based on pattern through redis client");
                var keys = client.GetKeysByPattern(keyPattern).ToList();
                Console.WriteLine($"Retrieved keys based on pattern through redis client");


                Console.WriteLine($"Start retrieving values through redis client");
                var values = client.GetValues(keys);
                Console.WriteLine($"Retrieved values through redis client");

                Console.WriteLine($"Start unwrapping redis values into object one by one");
                for (var i = 0; i < keys.Count; i++)
                {
                    string key = keys[i];
                    string valueJson = values[i];

                    RedisValueType wrapper = JsonConvert.DeserializeObject<RedisValueType>(valueJson);
                    var value = JsonConvert.DeserializeObject(wrapper.ValueJson, wrapper.ValueType);

                    result.Add(key, value);
                }
                Console.WriteLine($"Finish unwrapping redis values into object");
            }

            Console.WriteLine($"Get All");
            return result;
        }

        private Dictionary<string, JToken> _GetAllAsJToken(string keyPattern)
        {
            Dictionary<string, JToken> result = new Dictionary<string, JToken>();
            using (var client = _redisPool.GetManagerPool().GetReadOnlyClient())
            {
                Console.WriteLine($"Use Host: {client.Host}:{client.Port}");
                Console.WriteLine($"Get Role: {client.GetServerRole()}");

                var keys = client.GetKeysByPattern(keyPattern).ToList();
                var values = client.GetValues(keys);

                for (var i = 0; i < keys.Count; i++)
                {
                    string key = keys[i];
                    string valueJson = values[i];

                    JToken jToken = JToken.Parse(valueJson);
                    string jsonValue = jToken.SelectToken(nameof(RedisValueType.ValueJson)).Value<string>();
                    var value = JToken.Parse(jsonValue);

                    result.Add(key, value);
                }
            }

            Console.WriteLine($"Get All");
            return result;
        }
        #endregion
    }
}
