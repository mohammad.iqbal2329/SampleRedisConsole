#  @Description:
#    Entrypoint script for deploying redis HA via Sentinel in a kubernetes cluster
#    This script expects following environment variables to be set,
#    1. SENTINEL: true if this is sentinel instance, else false.
#    2. MASTER: true if this is master instance, this is helpful when starting the cluster for the first time.
#    3. REDIS_SENTINEL_SERVICE_SERVICE_HOST: this is service name of sentinel, check the yaml.
#    4. REDIS_SENTINEL_SERVICE_SERVICE_PORT: this is service port of sentinel.
#    5. REDIS_MASTER_SERVICE_SERVICE_HOST: this is master's service name, this is needed when sentinel starts for the first time.
#    6. REDIS_MASTER_SERVICE_SERVICE_PORT: this is master's port, is needed when sentinel starts for the first time.
#    7. REDIS_MASTER_GROUP_NAME: this is master group name, to differentiate between instances, i.e. xbase, masba-intranet, masba2-intranet, etc
#    8. QUORUM: this is the number of Sentinels that need to agree about the fact the master is not reachable.
#    9. DOWN_AFTER_MILLISECONDS: this is the time in milliseconds an instance should not be reachable.
#   10. FAILOVER_TIMEOUT: Sentinel voted another Sentinel for the failover of a given master, it will wait some time to try to failover the same master again. This delay is the 2 * failover-timeout

# Check Env Variable if empty will set to default value
function checkEnvVariableSentinel(){

	if [ -z "${QUORUM}" ]; then
		echo "QUORUM is empty..."
		QUORUM=2
		echo "set QUORUM to default ${QUORUM}"
	else
		echo "QUORUM is not empty..."
		echo "set QUORUM to default ${QUORUM}"
	fi
	
	if [ -z "${DOWN_AFTER_MILLISECONDS}" ]; then
		echo "DOWN_AFTER_MILLISECONDS is empty..."
		DOWN_AFTER_MILLISECONDS=5000
		echo "set DOWN_AFTER_MILLISECONDS to default ${DOWN_AFTER_MILLISECONDS}"
	else
		echo "DOWN_AFTER_MILLISECONDS is not empty..."
		echo "set DOWN_AFTER_MILLISECONDS to default ${DOWN_AFTER_MILLISECONDS}"
	fi
	
	if [ -z "${FAILOVER_TIMEOUT}" ]; then
		echo "FAILOVER_TIMEOUT is empty..."
		FAILOVER_TIMEOUT=60000
		echo "set FAILOVER_TIMEOUT to default ${FAILOVER_TIMEOUT}"
	else
		echo "FAILOVER_TIMEOUT is not empty..."
		echo "set FAILOVER_TIMEOUT to default ${FAILOVER_TIMEOUT}"
	fi
}

function checkEnvVariableMasterService(){

	if [ -z "${REDIS_MASTER_SERVICE_SERVICE_HOST}" ]; then
		echo "REDIS_MASTER_SERVICE_SERVICE_HOST is empty..."
		hostname=$(hostname -f)
        getStatefulsetName=$(echo ${hostname} | tr -s '.' ' ' | cut -d' ' -f2)
		getPodName=$(echo ${hostname} | tr -s '.' ' ' | cut -d' ' -f1)
		
        hostservice=$(echo ${hostname} | sed -r 's/.'${getStatefulsetName}'././' | sed -r 's/'${getPodName}'./redis-app-0./')
		REDIS_MASTER_SERVICE_SERVICE_HOST=${hostservice}
		echo "set REDIS_MASTER_SERVICE_SERVICE_HOST to default ${REDIS_MASTER_SERVICE_SERVICE_HOST}"
	else
		echo "REDIS_MASTER_SERVICE_SERVICE_HOST is not empty..."
		echo "set REDIS_MASTER_SERVICE_SERVICE_HOST to default ${REDIS_MASTER_SERVICE_SERVICE_HOST}"
	fi
	
	if [ -z "${REDIS_MASTER_SERVICE_SERVICE_PORT}" ]; then
		echo "REDIS_MASTER_SERVICE_SERVICE_PORT is empty..."
		REDIS_MASTER_SERVICE_SERVICE_PORT=6379
		echo "set REDIS_MASTER_SERVICE_SERVICE_PORT to default ${REDIS_MASTER_SERVICE_SERVICE_PORT}"
	else
		echo "REDIS_MASTER_SERVICE_SERVICE_PORT is not empty..."
		echo "set REDIS_MASTER_SERVICE_SERVICE_PORT to default ${REDIS_MASTER_SERVICE_SERVICE_PORT}"
	fi
}

function checkEnvVariableSentinelService(){

	if [ -z "${REDIS_SENTINEL_SERVICE_SERVICE_HOST}" ]; then
		echo "REDIS_SENTINEL_SERVICE_SERVICE_HOST is empty..."
		hostname=$(hostname -f)
        getStatefulsetName=$(echo ${hostname} | tr -s '.' ' ' | cut -d' ' -f2)
		getPodName=$(echo ${hostname} | tr -s '.' ' ' | cut -d' ' -f1)
		
        hostservice=$(echo ${hostname} | sed -r 's/.'${getStatefulsetName}'././' | sed -r 's/'${getPodName}'./redis-sentinel-app./')
		REDIS_SENTINEL_SERVICE_SERVICE_HOST=${hostservice}
		echo "set REDIS_SENTINEL_SERVICE_SERVICE_HOST to default ${REDIS_SENTINEL_SERVICE_SERVICE_HOST}"
	else
		echo "REDIS_SENTINEL_SERVICE_SERVICE_HOST is not empty..."
		echo "set REDIS_SENTINEL_SERVICE_SERVICE_HOST to default ${REDIS_SENTINEL_SERVICE_SERVICE_HOST}"
	fi
	

	if [ -z "${REDIS_SENTINEL_SERVICE_SERVICE_PORT}" ]; then
		echo "REDIS_SENTINEL_SERVICE_SERVICE_PORT is empty..."
		REDIS_SENTINEL_SERVICE_SERVICE_PORT=26379
		echo "set REDIS_SENTINEL_SERVICE_SERVICE_PORT to default ${REDIS_SENTINEL_SERVICE_SERVICE_PORT}"
	else
		echo "REDIS_SENTINEL_SERVICE_SERVICE_PORT is not empty..."
		echo "set REDIS_SENTINEL_SERVICE_SERVICE_PORT to default ${REDIS_SENTINEL_SERVICE_SERVICE_PORT}"
	fi
}

function checkEnvVariableMasterGroupName(){

	if [ -z "${REDIS_MASTER_GROUP_NAME}" ]; then
		echo "REDIS_MASTER_GROUP_NAME is empty..."
		REDIS_MASTER_GROUP_NAME="mymaster"
		echo "set REDIS_MASTER_GROUP_NAME to default ${REDIS_MASTER_GROUP_NAME}"
	else
		echo "REDIS_MASTER_GROUP_NAME is not empty..."
		echo "set REDIS_MASTER_GROUP_NAME to default ${REDIS_MASTER_GROUP_NAME}"
	fi
}
#End check Env Variable

#  This method launches redis instance which assumes it self as master
function launchmaster() {
  echo "Starting Redis instance as Master.."
  
  checkEnvVariableMasterService
  
  sed -i "s/%hostname-service%/${REDIS_MASTER_SERVICE_SERVICE_HOST}/" /redis-master/redis.conf
  sed -i "s/%hostname-port%/6379/" /redis-master/redis.conf
  
  redis-server /redis-master/redis.conf --protected-mode no

}

#  This method launches sentinels
function launchsentinel() {
  echo "Starting Sentinel.."

  while true; do
    echo "Trying to connect to Sentinel Service"
	
	checkEnvVariableSentinelService
	checkEnvVariableMasterGroupName
	
    echo "set to sentinel ${REDIS_SENTINEL_SERVICE_SERVICE_HOST} and ${REDIS_SENTINEL_SERVICE_SERVICE_PORT} with group name ${REDIS_MASTER_GROUP_NAME}"
    master=$(redis-cli -h ${REDIS_SENTINEL_SERVICE_SERVICE_HOST} -p ${REDIS_SENTINEL_SERVICE_SERVICE_PORT} --csv SENTINEL get-master-addr-by-name ${REDIS_MASTER_GROUP_NAME} | tr ',' ' ' | cut -d' ' -f1)
    
	if [[ -n ${master} ]]; then
      echo "Connected to Sentinel Service and retrieved Redis Master IP as ${master}"
      master="${master//\"}"
    else
	  
	  checkEnvVariableMasterService
	  
      echo "Unable to connect to Sentinel Service, probably because I am first Sentinel to start. I will try to find hostname from the redis service"
      echo "set to master ${REDIS_MASTER_SERVICE_SERVICE_HOST} and ${REDIS_MASTER_SERVICE_SERVICE_PORT}"
	  
	  role=$(echo $(redis-cli -h ${REDIS_MASTER_SERVICE_SERVICE_HOST} -p ${REDIS_MASTER_SERVICE_SERVICE_PORT} role) | tr -s ' ' ' ' | cut -d' ' -f1)
	  if [[ -n ${role} ]]; then
		  echo "Role is not empty: ${role}"
		  if [[ "${role}" == "master" ]]; then
			master=${REDIS_MASTER_SERVICE_SERVICE_HOST}
			echo "set variable master to ${master}"
		  else
			master=$(echo $(redis-cli -h ${REDIS_MASTER_SERVICE_SERVICE_HOST} -p ${REDIS_MASTER_SERVICE_SERVICE_PORT} role) | tr -s ' ' ' ' | cut -d' ' -f2)
			echo "set variable master to ${master}"
		  fi
	  else
		echo "Role still empty. Waiting..."
        sleep 10
        continue
	  fi
    fi
	
    redis-cli -h ${master} INFO
    if [[ "$?" == "0" ]]; then
      break
    fi
    echo "Connecting to master failed.  Waiting..."
    sleep 10
  done

  sentinel_conf=/redis-sentinel/sentinel.conf
  
  hostname=$(echo $(hostname -f) | tr -s '-' ' ' | tr -s '.' ' ')
  hostname=${hostname//[[:space:]]/}
  
  checkEnvVariableSentinel
  
  echo "sentinel resolve-hostnames yes" >> ${sentinel_conf}
  echo "sentinel announce-hostnames yes" >> ${sentinel_conf}
  echo "sentinel myid ${hostname:0:40}" >> ${sentinel_conf}
  echo "sentinel monitor ${REDIS_MASTER_GROUP_NAME} ${master} 6379 ${QUORUM}" >> ${sentinel_conf}
  echo "sentinel down-after-milliseconds ${REDIS_MASTER_GROUP_NAME} ${DOWN_AFTER_MILLISECONDS}" >> ${sentinel_conf}
  echo "sentinel failover-timeout ${REDIS_MASTER_GROUP_NAME} ${FAILOVER_TIMEOUT}" >> ${sentinel_conf}
  echo "sentinel parallel-syncs ${REDIS_MASTER_GROUP_NAME} 1" >> ${sentinel_conf}
  echo "bind 0.0.0.0" >> ${sentinel_conf}

  redis-sentinel ${sentinel_conf} --protected-mode no
}

#  This method launches slave instances
function launchslave() {
  echo "Starting Redis instance as Slave , Master IP $1"

  while true; do
    echo "Trying to retrieve the Master IP again, in case of failover master ip would have changed."
	
	checkEnvVariableSentinelService
	checkEnvVariableMasterGroupName
	
    echo "set to sentinel ${REDIS_SENTINEL_SERVICE_SERVICE_HOST} and ${REDIS_SENTINEL_SERVICE_SERVICE_PORT} with group name ${REDIS_MASTER_GROUP_NAME}"
    master=$(redis-cli -h ${REDIS_SENTINEL_SERVICE_SERVICE_HOST} -p ${REDIS_SENTINEL_SERVICE_SERVICE_PORT} --csv SENTINEL get-master-addr-by-name ${REDIS_MASTER_GROUP_NAME} | tr ',' ' ' | cut -d' ' -f1)
    if [[ -n ${master} ]]; then
      master="${master//\"}"
    else
      echo "Failed to find master."
      sleep 10
      continue
    fi
    redis-cli -h ${master} INFO
    if [[ "$?" == "0" ]]; then
      break
    fi
    echo "Connecting to master failed.  Waiting..."
    sleep 10
  done
  
  hostname=$(hostname -f)
  getStatefulsetName=$(echo ${hostname} | tr -s '.' ' ' | cut -d' ' -f2)
  hostservice=$(echo ${hostname} | sed -r 's/.'${getStatefulsetName}'././')

  echo "I will connect to announce-hostnames ${hostservice}"
  
  sed -i "s/%hostname-service%/${hostservice}/" /redis-slave/redis.conf
  sed -i "s/%hostname-port%/6379/" /redis-slave/redis.conf

  sed -i "s/%master-ip%/${master}/" /redis-slave/redis.conf
  sed -i "s/%master-port%/6379/" /redis-slave/redis.conf
  
  redis-server /redis-slave/redis.conf --protected-mode no
}


#  This method launches either slave or master based on some parameters
function launchredis() {
  echo "Launching Redis instance"
  
  # Loop till I am able to launch slave or master
  while true; do
    # I will check if sentinel is up or not by connecting to it.
    echo "Trying to connect to sentinel, to retireve master's ip"
	
	checkEnvVariableSentinelService
	checkEnvVariableMasterGroupName
	
    echo "set to sentinel ${REDIS_SENTINEL_SERVICE_SERVICE_HOST} and ${REDIS_SENTINEL_SERVICE_SERVICE_PORT} with group name ${REDIS_MASTER_GROUP_NAME}"
    master=$(redis-cli -h ${REDIS_SENTINEL_SERVICE_SERVICE_HOST} -p ${REDIS_SENTINEL_SERVICE_SERVICE_PORT} --csv SENTINEL get-master-addr-by-name ${REDIS_MASTER_GROUP_NAME} | tr ',' ' ' | cut -d' ' -f1)
	
    # If I am not master, then i am definitely slave.
    if [[ -n ${master} ]]; then
      echo "Connected to Sentinel and Retrieved Master IP ${master}"
      launchslave ${master}
      exit 0
    else
      echo "Connecting to sentinel failed, Waiting..."
      sleep 10
    fi
  done
}

# Main Entrypoint
if [[ "${MASTER}" == "true" ]]; then
	launchmaster
	exit 0
fi

if [[ "${SENTINEL}" == "true" ]]; then
	launchsentinel
	exit 0
fi

launchredis
# End Main Entrypoint
